import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_client/src/bloc/commits/commits_bloc.dart';
import 'package:github_client/src/bloc/commits/commits_event.dart';
import 'package:github_client/src/bloc/commits/commits_state.dart';
import 'package:github_client/src/models/commits.dart';

class CommitsPage extends StatefulWidget {
  @override
  CommitsPageState createState() => CommitsPageState();
  final String userName;
  final String repositoryName;

  CommitsPage(this.userName, this.repositoryName, {Key key}) : super(key: key);
}

class CommitsPageState extends State<CommitsPage> {
  CommitsBloc _commitsBloc;

  @override
  void initState() {
    super.initState();
    _commitsBloc = CommitsBloc();
    findUser();
  }

  findUser() {
    _commitsBloc.add(
      FetchCommits(
        userName: widget.userName,
        repositoryName: widget.repositoryName,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: BlocBuilder<CommitsBloc, CommitsState>(
        bloc: _commitsBloc,
        builder: (context, state) {
          if (state is CommitsError) {
            return Center(
              child: Text(state.error),
            );
          }
          if (state is CommitsLoading) {
            return Material(child: Center(child: CircularProgressIndicator()));
          }
          if (state is CommitsLoaded) {
            return Material(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return CommitWidget(commit: state.commits[index]);
                },
                itemCount: state.commits.length,
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class CommitWidget extends StatelessWidget {
  final Commits commit;
  const CommitWidget({Key key, @required this.commit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        leading: CircleAvatar(
          backgroundImage: NetworkImage(commit.avatar != null
              ? commit.avatar
              : 'https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png'),
        ),
        title: Text(commit.author != null ? commit.author : 'нет имени',
            style: TextStyle(fontWeight: FontWeight.bold)),
        trailing: Text(commit.sha.substring(0, 7)),
        subtitle: Text(commit.commitMessage),
        dense: true,
      ),
    );
  }
}
