import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:github_client/src/bloc/repository/repository_bloc.dart';
import 'package:github_client/src/bloc/repository/repository_event.dart';
import 'package:github_client/src/bloc/repository/repository_state.dart';
import 'package:github_client/src/models/repository.dart';

import 'commits_page.dart';

class RepositoriesPage extends StatefulWidget {
  @override
  RepositoriesPageState createState() => RepositoriesPageState();
}

class RepositoriesPageState extends State<RepositoriesPage> {
  final _usernameController = TextEditingController();
  RepositoryBloc _repositoryBloc;

  @override
  void initState() {
    super.initState();
    _repositoryBloc = RepositoryBloc();
  }

  findUser() {
    _repositoryBloc.add(
      FindUser(username: _usernameController.text),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BlocBuilder<RepositoryBloc, RepositoryState>(
        bloc: _repositoryBloc,
        builder: (context, state) {
          return Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        controller: _usernameController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Enter username',
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Material(
                        color: Colors.white,
                        child: Center(
                          child: Ink(
                            decoration: const ShapeDecoration(
                              color: Colors.lightBlue,
                              shape: CircleBorder(),
                            ),
                            child: IconButton(
                              icon: Icon(Icons.search),
                              color: Colors.white,
                              onPressed: findUser,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                _buildChild(state, context)
              ],
            ),
          );
        },
      )),
    );
  }
}

Widget _buildChild(state, context) {
  if (state is RepositoryError) {
    return Center(
      child: Text(
        state.error != null ? state.error : 'gg',
        textAlign: TextAlign.center,
      ),
    );
  }
  if (state is RepositoryLoading) {
    return Center(child: CircularProgressIndicator());
  }
  if (state is RepositoryLoaded) {
    print('state ${state.repositories.length}');
    return Expanded(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return RepositoryWidget(repository: state.repositories[index]);
        },
        itemCount: state.repositories.length,
      ),
    );
  }
  return Center(
    child: Text('Начните поиск пользователей gitHub'),
  );
}

class RepositoryWidget extends StatelessWidget {
  final Repository repository;

  const RepositoryWidget({Key key, @required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        FocusScope.of(context).unfocus();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => CommitsPage(repository.author, repository.repositoryName)));
      },
      leading: CircleAvatar(
        backgroundImage: NetworkImage(repository.avatar),
      ),
      title: Text(repository.fullName),
      trailing: Text('stars ${repository.stargazers.toString()}'),
      subtitle: Text(
          repository.description != null ? repository.description : 'Описание отсутствует'),
      dense: true,
    );
  }
}
