import 'package:flutter/material.dart';
import 'package:github_client/src/pages/repositories_page.dart';

class App extends StatefulWidget {
  @override
  State createState() => new MyWidgetState();
}

class MyWidgetState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: RepositoriesPage());
  }
}
