import 'package:equatable/equatable.dart';
import 'package:github_client/src/models/commits.dart';


abstract class CommitsState extends Equatable {
  const CommitsState();

  @override
  List<Object> get props => [];
}

class CommitsUninitialized extends CommitsState {}

class CommitsError extends CommitsState {
  final String error;

  const CommitsError({
    this.error,
  });


  @override
  List<Object> get props => [error];
}

class CommitsLoading extends CommitsState {}

class CommitsLoaded extends CommitsState {
  final List<Commits> commits;


  const CommitsLoaded({
    this.commits,
  });


  @override
  List<Object> get props => [commits];

  @override
  String toString() => 'CommitsLoaded { commitsLengt: ${commits.length}}';
}

