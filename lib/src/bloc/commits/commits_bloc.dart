import 'dart:async';
import 'dart:convert';
import 'package:github_client/src/models/commits.dart';
import 'commits_event.dart';
import 'commits_state.dart';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';

class CommitsBloc extends Bloc<CommitsEvent, CommitsState> {
  @override
  get initialState => CommitsUninitialized();

  @override
  Stream<CommitsState> mapEventToState(CommitsEvent event) async* {
    if (event is FetchCommits) {
      try {
        yield CommitsLoading();
        final commits = await _fetchCommits(
            userName: event.userName, repositoryName: event.repositoryName);
        yield CommitsLoaded(commits: commits);
        return;
      } catch (e) {
        print('error--------------$e');
        yield CommitsError(
            error: e != 'isEmpty'
                ? 'Ошибка запроса данных!'
                : 'Пустой репозиторий');
      }
    }
  }

  Future<List<Commits>> _fetchCommits(
      {String userName, String repositoryName}) async {
    final response = await http.Client().get(
        'https://api.github.com/repos/$userName/$repositoryName/commits?per_page=5');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      return data.map((commit) {
        return Commits(
          sha: commit['sha'],
          commitMessage: commit['commit']['message'],
          author: commit['author'] != null
              ? commit['author']['login']
              : commit['commit']['author']['name'],
          avatar:
              commit['author'] != null ? commit['author']['avatar_url'] : null,
        );
      }).toList();
    } else {
      throw 'isEmpty';
    }
  }
}
