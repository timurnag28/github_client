import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';


abstract class CommitsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchCommits extends CommitsEvent {
  final String userName;
  final String repositoryName;

  FetchCommits({
    @required this.userName, this.repositoryName
  });

  @override
  List<Object> get props => [userName, repositoryName];

  @override
  String toString() => 'FetchCommits { username: $userName} repositoryName: $repositoryName';
}


