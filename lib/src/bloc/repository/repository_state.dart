import 'package:equatable/equatable.dart';
import 'package:github_client/src/models/repository.dart';


abstract class RepositoryState extends Equatable {
  const RepositoryState();

  @override
  List<Object> get props => [];
}

class RepositoryUninitialized extends RepositoryState {}

class RepositoryError extends RepositoryState {
  final String error;

  const RepositoryError({
    this.error,
  });

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RepositoryLoaded { error: $error}';
}

class RepositoryLoading extends RepositoryState {}

class RepositoryLoaded extends RepositoryState {
  final List<Repository> repositories;

  const RepositoryLoaded({
    this.repositories,
  });

  @override
  List<Object> get props => [repositories];

  @override
  String toString() => 'RepositoryLoaded { Repositories: ${repositories.length}}';
}
