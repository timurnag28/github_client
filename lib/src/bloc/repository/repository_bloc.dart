import 'dart:async';
import 'dart:convert';

import 'package:github_client/src/bloc/repository/repository_event.dart';
import 'package:github_client/src/bloc/repository/repository_state.dart';
import 'package:github_client/src/models/repository.dart';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';

class RepositoryBloc extends Bloc<RepositoryEvent, RepositoryState> {
  @override
  get initialState => RepositoryUninitialized();

  @override
  Stream<RepositoryState> mapEventToState(RepositoryEvent event) async* {
    if (event is FindUser) {
      try {
        yield RepositoryLoading();
        final repositories = await _fetchRepositories(event.username);
        yield RepositoryLoaded(repositories: repositories);
        return;
      } catch (e) {
        print('error--------------$e');
        if(e == 'Not Found'){
          yield RepositoryError(
              error: 'Ничего не найдено');
          return;
        }
        if(e == 'limit'){
          yield RepositoryError(
              error: 'Лимит запросов превышен');
          return;
        }
        yield RepositoryError(
            error: 'Ошибка запроса, возможно нет интернета');
        return;

      }
    }
  }

  Future<List<Repository>> _fetchRepositories(String userName) async {
    print('https://api.github.com/users/$userName/repos');
    final response =
        await http.Client().get('https://api.github.com/users/$userName/repos');
    if (response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      return data.map((repository) {
        return Repository(
          id: repository['id'],
          repositoryName: repository['name'],
          fullName: repository['full_name'],
          description: repository['description'],
          stargazers: repository['stargazers_count'],
          author: repository['owner']['login'],
          avatar: repository['owner']['avatar_url'],
        );
      }).toList();
    } else {
      Map<String, dynamic> message = jsonDecode(response.body);
      var error = message['message'];
      throw error != 'Not Found' ? 'limit' : 'Not Found';
    }
  }
}
