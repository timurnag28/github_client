import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class RepositoryEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class Fetch extends RepositoryEvent {}


class FindUser extends RepositoryEvent {
  final String username;

  FindUser({
    @required this.username,
  });

  @override
  List<Object> get props => [username];

  @override
  String toString() => 'FindUser { username: $username}';
}
