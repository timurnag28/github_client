import 'package:equatable/equatable.dart';

class Commits extends Equatable {
  final String sha;
  final String commitMessage;
  final String author;
  final String avatar;

  const Commits({this.sha, this.commitMessage, this.author, this.avatar});

  @override
  List<Object> get props => [sha, commitMessage,author,avatar];

  @override
  String toString() => 'Commit { commitMessage: $commitMessage }';
}
