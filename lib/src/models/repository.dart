import 'package:equatable/equatable.dart';

class Repository extends Equatable {
  final int id;
  final String repositoryName;
  final String fullName;
  final String description;
  final int stargazers;
  final String author;
  final String avatar;

  const Repository({this.id, this.repositoryName, this.description, this.fullName, this.stargazers, this.author, this.avatar});

  @override
  List<Object> get props => [id, repositoryName, description,fullName, stargazers, author, avatar];

  @override
  String toString() => 'Repository { id: $id }';
}
